from flask import Flask
from redis import Redis
import os
import socket
import signal
from multiprocessing import Process

host_run = os.environ.get('HOST_RUN', '0.0.0.0');
debug = os.environ.get('DEBUG', 'true');

host_redis = os.environ.get('HOST_REDIS', 'redis');
port_redis = os.environ.get('PORT_REDIS', 6379);

app = Flask(__name__);
redis = Redis(host=host_redis, port=port_redis);

@app.route('/')
def hits():
    redis.incr('hits');
    return({ "Hits": str(redis.get('hits')), "Hostname": str(socket.gethostname()) });

if __name__ == "__main__":

    def server_handler(signum, frame):
        server.terminate();
        server.join();

    signal.signal(signal.SIGTERM, server_handler);

    def run_server():
        app.run(host=host_run, debug=debug);

    server = Process(target=run_server);
    server.start();
