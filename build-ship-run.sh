#!/bin/bash

USER="lsantana"
TIMESTAMP=$(date "+%Y.%m.%d-%H.%M")
IMAGE_NAME="base-api-flask"

REPOSITORY_VERSION="${USER}/${IMAGE_NAME}:${TIMESTAMP}"
REPOSITORY_LATEST="${USER}/${IMAGE_NAME}:latest"

docker build -t ${REPOSITORY_VERSION} .

docker tag ${REPOSITORY_VERSION} ${REPOSITORY_LATEST}

docker push ${REPOSITORY_VERSION}
docker push ${REPOSITORY_LATEST}

