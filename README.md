Base API - 12FactorApp
---

Aplicação base para criação de APIs utilizando os padrões de desenvolvimento [The Twelve Factor App](https://12factor.net/pt_br/). Infraestrutura utilizando [docker](https://www.docker.com/) o que permite escalar a aplicação de forma simples, quando necessário.

Diagrama da Infraestrutura

![Diagrama de infraestrutura](./images/api-base-12factorapp.png)

## Como utilizar

1. Com o intuito de simular um servidor de logs externo, foi criado, dentro da pasta [log-server-graylog](./log-server-graylog/), um arquivo [docker-compose](log-server-graylog/docker-compose.yaml) que permite levantar um servidor [graylog](https://www.graylog.org). O docker-compose foi coletado diretamente da [documentação oficial do Graylog](https://docs.graylog.org/en/3.3/pages/installation/docker.html).

   ```bash
   cd log-server-graylog && docker-compose up -d && cd -
   ```

2. Aguarde o servidor levantar e pode ser acessado via [localhost](localhost) (usuário: admin, senha: admin).
3. Acesse o servidor insira um novo INPUT do tipo GELF UDP para escutar de forma global todos os logs enviados.
4. Execute a aplicação da API base especificando o IP do servidor de logs

   ```bash
   ENV_APP="staging" GELF_HOST="seu-ip" GELF_PORT="12201" docker-compose up -d --scale api=5
   ```

   > Sinta-se a vontade para brincar com as variáveis especificadas. Assim como com o número de nós que a aplicação permite escalar.
